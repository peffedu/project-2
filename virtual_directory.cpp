#include <iostream>
#include "bst.h"
#include "directory.h"

using namespace std;


/* Change `cwd` to point to either its parent (..) or one of its
   subdirectories.

   If `name` is `..` change into the parent directory. Attempting to change to
   the parent directory of the root should produce the "Invalid directory:
   <NAME>" error message.

   Otherwise `cwd` should only be updated if `cwd` has a subdirectory with the
   name `name`. Otherwise the message "Invalid directory: <NAME>" should be
   output where `<NAME>` is replaced by the `name` variable.

   Note: `cwd` is a reference to a pointer so that when you reassign `cwd` that
   change occurs for the cwd of the caller (main in this case).
*/
void change_directory(Directory *&cwd, string name) {
	if(name == ".." && cwd->get_parent() == NULL){
		cout << "Invalid directory: " << name << "\n";
		return;
		}	
	if(name == ".."){
		cwd = cwd->get_parent();
		return;
		}
	else{
		if(cwd->get_subdirectory(name) != NULL){
			cwd = cwd->get_subdirectory(name);
			return;
			}
		else{
			cout << "Invalid directory: " << name << "\n";
			return;
			}
		}
  // TODO: Implement this function
}


/* Attempt to create a directory with name `name` under the cwd.

   The directory name `..` is reserved to mean the parent directory. If `name`
   matches `..` output: ".. is a reserved directory name\n"

   On failure (the directory already exists) output: "Directory already exists:
   <NAME>" where `<NAME>` is replaced by the `name` variable.
*/
void make_directory(Directory *cwd, string name) {
	if(name == "..")
	{
		std::cout << ".. is a reserved directory name\n";
		return;
	}
	if(cwd->get_subdirectory(name) != NULL){
		std::cout << "Directory already exists: " << name << "\n";
		return;
	}
	cwd->add_subdirectory(name);
  // TODO: Implement this function
}


/* Output the complete path to the present working directory.

   From the root the output should only be "/\n".

   From a directory foo with parent bar whose parent is the root the output
   should be "/bar/foo\n".

   Hint: You might want to use one of the data structures to help produce the
   output in the correct order.
*/
void output_pwd(Directory *cwd) {
    // TODO: Implement this function
    if (cwd->get_name() == ""){
        std::cout << "/\n";
        return;
    }
    Directory *tmp = cwd;
    Stack < basic_string<char> > s;
    s.push(cwd->get_name());
    while (tmp->get_parent()->get_name() != "") {
        s.push(tmp->get_parent()->get_name());
        tmp = tmp->get_parent();
    }
    while (!s.is_empty())
        std::cout << "/" << s.pop();
    std::cout << "\n";
	return;
}

/* Attempt to remove a subdirectory of `cwd` matching `name`.

   If the directory does not exist output: "Invalid directory: <NAME>\n"
*/
void remove_directory(Directory *cwd, string name) {
	Directory *tmp = cwd;
	if(tmp->get_subdirectory(name) == NULL){
		std::cout << "Invalid directory: " << name << "\n";
		}
	else{
		tmp->remove_subdirectory(name);
		}
	return;		
  // TODO: Implement this function
}


/* The main program simply prompts the user for commands until the input stream
   is closed (ctrl+d or EOF).

   You need not change anything in main. However, you are more than welcome to
   add additional commands for fun and possibly extra credit.
*/
int main() {
  Directory root("");
  Directory *cwd = &root;
  string command;

  while (cin) {
    cout << cwd->get_name() << "> ";
    getline(cin, command);
    if (command == "help") {
      cout << "Commands: cd NAME, mkdir NAME, ls, pwd, rmdir NAME\n";
    }
    else if (command.substr(0, 3) == "cd ") {
      change_directory(cwd, command.substr(3, command.size()));
    }
    else if (command == "ls") {
      cwd->list_directory();
    }
    else if (command.substr(0, 6) == "mkdir ") {
      make_directory(cwd, command.substr(6, command.size()));
    }
    else if (command == "pwd") {
      output_pwd(cwd);
    }
    else if (command.substr(0, 6) == "rmdir ") {
      remove_directory(cwd, command.substr(6, command.size()));
    }
    else if (command == "");
    else {
      cout << "Invalid command. Type help for the list of commands.\n";
    }
  }
  return 0;
}
