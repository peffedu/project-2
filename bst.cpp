#include <iostream>


template <class T>
BinaryNode<T> *BST<T>::find_max(BinaryNode<T> *node) {
  // TODO: Implement this function (when you have a use for it)
  return NULL;
}

template <class T>
BinaryNode<T>* removeR(T item, BinaryNode<T>* curr)
{
    BinaryNode<T> *currentNode = curr;
   if(item == currentNode->get_data()){
      return currentNode;
   } else if (item < currentNode->get_data()){
      removeR(item, currentNode->get_lhs());
   } else{
      removeR(item, currentNode->get_rhs());
   }
   return NULL;
}

template <class T>
T BST<T>::remove(T item) {
    BinaryNode<T> *removeThis = removeR(item, root);
    if(removeThis == NULL)
    {
     throw 1;
    }
    if(removeThis->get_lhs() == NULL)
    {
     root = removeThis->get_rhs();
    }
    else
    {
     root = removeThis->get_lhs();
    }
	T re = removeThis->get_data();
	delete removeThis;
    return re;
    throw 1;
}


template <class T>
T BST<T>::remove_root() {
  // TODO: Implement this function
  throw 1;
}

template <class T>
void BST<T>::sorted_output() const {
    // TODO: Implement this function
    Stack<BinaryNode<T>*> s;
    BinaryNode<T> *current = root;
    bool done = false;
	int count = 0;
    while (!done)
    {
        if (current != NULL) {
            s.push(current);
            current = current->get_lhs();
        }
        else {
            if (!s.is_empty()) {
                current = s.pop();
                std::cout << current->get_data() << " ";
                current = current->get_rhs();
				count++;
            }
            else {
                done = true;
				if(count != 0)
					std::cout << "\n";
            }
        }
    }
	return;
}

template <class T>
BST<T>::~BST() {
  // TODO: Copy your lab7 implementation here
	BinaryNode<T> *tmp;
    BinaryNode<T> *prev;
    int direction = 0;
    while (root != NULL) {
        tmp = root;
        while (tmp->get_lhs() != NULL) {
            prev = tmp;
            tmp = tmp->get_lhs();
            direction = 0;
        }
        while (tmp->get_rhs() != NULL) {
            prev = tmp;
            tmp = tmp->get_rhs();
            direction = 1;
        }
        if (tmp != root) {
            if (direction == 0)
                prev->set_lhs(NULL);
            else
                prev->set_rhs(NULL);
            delete tmp;
        }
        if ((root->get_lhs() == NULL) && (root->get_rhs() == NULL)) {
            delete root;
            root = NULL;
        }
    }
}

template <class T>
bool BST<T>::insert(T item) {
  // TODO: Copy your lab7 implementation here
  BinaryNode<T> *to_insert = new BinaryNode<T>(item, NULL, NULL);
    if (root == NULL) {
        root = to_insert;
        return true;
    }
    BinaryNode<T> *tmp;
    tmp = root;
    while (tmp != NULL) {
        if (item == root->get_data()) {
            delete to_insert;
            return false;
        }
        if (item < tmp->get_data()) {
            if (tmp->get_lhs() == NULL) {
                tmp->set_lhs(to_insert);
                return true;
            }
            tmp = tmp->get_lhs();
            if (item == tmp->get_data()) {
                delete to_insert;
                return false;
            }
        }
        else if (item > tmp->get_data()) {
            if (tmp->get_rhs() == NULL) {
                tmp->set_rhs(to_insert);
                return true;
            }
            tmp = tmp->get_rhs();
            if (item == tmp->get_data()) {
                delete to_insert;
                return false;
            }
        }
    }

    return false;
}
